
## v0.2.0 - 2018-04-07
- Add option to nest directories by category (#1)
- Store challenge descriptions (#2)
- Store table of contents in challenges.md (#3)
- Changed default behavior to put all files in a directory (less surprising)

## v0.1.0 - 2018-04-06
- Initial release
- Just downloaded files
