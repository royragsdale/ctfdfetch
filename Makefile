
# Simple Makefile to handle packaging and release to pypi

package:
	python3 setup.py sdist
	python3 setup.py bdist_wheel

upload: package
	twine upload dist/*

clean:
	rm -rf ctfdfetch.egg-info
	rm -rf dist
	rm -rf build
